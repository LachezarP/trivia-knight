# 3.0.0 Third Milestone
Added - Planned
* Added Item class with its methods
* Added Replayability (Multiple levels and increasment of difficulty)
* Added Death screen activity
* Added Upgrades to the character (once a level is completed)
* Added Multiple enemies that appear after certain level
* Added Passing information throught activities
* Added couple more images to the drawables

Improved
* Questions are now randomized and correct answers are on a random button.
* Fluidity throught levels
* Some buttons have their own texture
* Better design 
* Swapped from Alerts to Toasts for display of correct/incorrect answer

Note: 
* We were not able to figure out how to send LinkedList/Array List through intents,
therefore the Shop is still in development and items are stil not usable.

Known Bugs: 
* Sometimes when an emulator freshly installs the game, the first time you try to enter
into the Level screen, it will not contact the API. We suppose it's a internet/connectivity issue,
because if the app is restarted, it will work just fine.
* Some interferace with the Upgrades that make the Maximum health lower than the current health.(Still working on this)


# 2.0.0 Second Milestone
Added - Planned
* Added API connection to the app
* Added Recycler view with the items of the shop 
* Added Alert when the user picks the answer to the question
* Added Level 1 with finished game loop (Finishes when one of the players dies)
* Added and Adjusted the images for the attributes.
* Added more images into the drawables

Improved 
* Basic controls - Added further navigations within the settings and other buttons
* Improved Layouts and constraints 
* Added the code for inside the empty code

Notes: 
* Recycle view in shop works however the currency is not yet implemented as it
is not part of the important game loop. That being said, you can still manipulate the buttons.

Known Bugs:

* In this milestone we focused on finishing the "Happy road" which is the best case senario.
There is a bug that crashes the game if you go from Level One activity back to Select screen 
back to Level One activity (It will be fixed once a save on state is implemented)
* When question is generated, there is a chance that a right answer is not assigned. (Unknown for now)


# 1.0.0 First Milestone 
Added - Planned
* Added layouts with proper attributes such as buttons, textviews etc.
* Added drawables that will represent the attriutes.
* Decorated the layouts with basic images from android studio to structure the attributes
* Added empty code such as methods to layouts.
* Added strings and ids to refrence the attributes

Unplanned - Extra
* Added navigation throught screens.

