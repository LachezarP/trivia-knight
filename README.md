# Application Name
Trivia Knight

# Description
Trivia Knight is going to be a game situated in the medieval era about a knight 
that fights monsters by answering trivia question every time he tries to attack. 

# Guide 
The application has a menu view as soon as the user starts it. There will be a play 
button that if clicked, will proceed to the level/character selection. Beside the play 
button there is a menu button that will let the user swap settings. Once the play 
button is clicked, you enter into a level that each time a monster is being 
confronted, you will be prompted with questions. Answer them right by clicking the buttons
that will appear and you may successfully damage the enemy.

# API
The open Trivia API is used to create the random questions that are going 
to be asked through the game. The API will create a link with randomized questions
about medieval era and will generate multiple responses and correct answers.

# Usage
1. Choose character
2. Choose difficulty
3. Choose level
4. Answer the trivia questions right in order to damage the enemy
5. Display right answer
6. Display the total score

# Alternatives
1. There might be different characters that will join the fight. 
2. Different mechanics (such as introduction of usable items)

# Target users
The target users are people any one that 6 years or older

# Visuals
![image](Proposal/Mockups/MenuScreen.PNG)

![image](Proposal/Mockups/LevelExample.PNG)

![image](Proposal/Mockups/ChooseLevelScreen.PNG)


# Roadmap

1. The first milestone ( Start date: 8th November, Due date: 15th November ) : 
    We are looking to deliver a base version of the application. This will include 
all the layouts with basic images to point out the places of the actual drawables.
The strings will be included as well as an empty code that will structure the future
transitions and implementation. Finally a decoration of the layouts will be presented.

   * ISSUE 1: Layouts 
   * ISSUE 2: Strings 
   * ISSUE 3: Drawable 
   * ISSUE 4: Empty code 
   * ISSUE 5: Decorate 

2. The second milestone ( Start date: 15th November, Due date: 22th November ) :
    We are looking to deliver the basic controls such as transitions from screens and
working buttons. We will establish a connection with the API and implement a recycler
view with the items for the shop activity. Finally, we will have alerts when the user quits the game
and the level 1 will be playable. 

   * ISSUE 1: Basic controls 
   * ISSUE 2: API 
   * ISSUE 3: Recycler view 
   * ISSUE 4: Alert 
   * ISSUE 5: Level 1 
   
3. The third milestones ( Start date: 22th November, Due date: 29th November ) :
    For the last deliverable we are aiming to have more than one level to truely test
the gaming loop. The settings will be implemented and they will help customize some of the aspects
such as the volume. Objects such as items and the shop will be able to provide more variety 
for the game loop. Finally the death screen with credits will be projected when a player loses.
   * ISSUE 1: Multiple Levels 
   * ISSUE 2: Settings
   * ISSUE 3: Items
   * ISSUE 4: Shop
   * ISSUE 5: Death screen

# Concepts
1. Recycler View for the levels
2. Volley to get the API
3. Preferences (to keep the information)
4. User Navigation to move around the game
5. Gifs for animation

# Authors
1. Lachezar Peychev - Developer
2. Hao Yuan Zhang - Developer
3. Bryan Diego-Rivas - Developer
