package com.example.triviaknight;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;

public class SelectScreen extends AppCompatActivity {
    Intent i;
    TextView goToLevelButton;

    Character heroChar;
    int extraArmor;
    int extraDamage;
    int extraHealth;
    ArrayList<Item> itemList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_screen);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        goToLevelButton = findViewById(R.id.goToLevelOneButton);
        i = getIntent();
        heroChar = (Character)i.getSerializableExtra("heroChar");
        itemList = (ArrayList<Item>)i.getSerializableExtra("itemList");
        extraArmor = i.getIntExtra("extraArmor", 0);
        extraDamage = i.getIntExtra("extraDamage", 0);
        extraHealth = i.getIntExtra("extraHealth", 0);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        goToLevelButton.setText("Level " + heroChar.getLevel());

        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.ingame_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent settingScreen = new Intent(this, Settings.class);
            startActivity(settingScreen);
            return true;
        }
        if (id == R.id.action_shop) {
            Intent intent = new Intent(SelectScreen.this, ShopScreen.class);
            startActivityForResult(intent, 0);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void goToLevelOne(View view) {
        Intent levelOneScreen = new Intent(this, LevelOneScreen.class);

        levelOneScreen.putExtra("heroChar", heroChar);
        i.putExtra("extraArmor",extraArmor);
        i.putExtra("extraDamage",extraDamage);
        i.putExtra("extraHealth",extraHealth);
        i.putExtra("itemList", itemList);
        startActivityForResult(levelOneScreen, 1);
    }

    public void goToShop(View view) {
        Intent intent = new Intent(this, ShopScreen.class);
        intent.putExtra("heroChar", heroChar);
        i.putExtra("extraArmor",extraArmor);
        i.putExtra("extraDamage",extraDamage);
        i.putExtra("extraHealth",extraHealth);
        i.putExtra("itemList", itemList);
        startActivity(intent);
    }

    public void goToUpgrade(View view) {
        Intent intent = new Intent(this, Upgrade_Screen.class);
        intent.putExtra("heroChar", heroChar);
        i.putExtra("extraArmor",extraArmor);
        i.putExtra("extraDamage",extraDamage);
        i.putExtra("extraHealth",extraHealth);
        i.putExtra("itemList", itemList);
        startActivityForResult(intent, 1);
    }
}
