package com.example.triviaknight;

import android.app.ActionBar;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class LevelOneScreen extends AppCompatActivity {
    Random rand = new Random();
    TextView questionTextView;
    Button answerATextView;
    Button answerBTextView;
    Button answerCTextView;
    Button answerDTextView;
    ImageView heroImage;
    ImageView castleImage;
    ImageView monsterImage;
    ProgressBar healthHeroBar;
    ProgressBar healthMonsterBar;
    Character heroChar;
    Intent i;
    Character enemyChar;
    Toast toast;
    int extraHealth = 0;
    int extraArmor = 0;
    int extraDamage = 0;

    int correctIndex;
    private String questionDifficulty;
    private ArrayList<String> answers = new ArrayList<>();
    private ArrayList<Integer> correctAns = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level_one_screen);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        questionTextView = findViewById(R.id.cost_textView);
        answerATextView = findViewById(R.id.answerA_btn);
        answerBTextView = findViewById(R.id.answerB_btn);
        answerCTextView = findViewById(R.id.answerC_btn);
        answerDTextView = findViewById(R.id.answerD_btn);


        getQuestion();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        castleImage = findViewById(R.id.fightScreenImg);
        Glide.with(this).load(R.drawable.castle).into(castleImage);
        heroImage = findViewById(R.id.heroImg);
        Glide.with(this).load(R.drawable.knight_idle).into(heroImage);
        monsterImage = findViewById(R.id.monsterImg);
        Glide.with(this).load(R.drawable.enemy_idle).into(monsterImage);

        i = getIntent();
        heroChar = (Character)i.getSerializableExtra("heroChar");

        healthHeroBar = findViewById(R.id.healthHeroPbar);
        healthHeroBar.setMax(heroChar.getmHealth());
        healthHeroBar.setProgress(heroChar.getmCurrentHealth());

        healthMonsterBar = findViewById(R.id.healthEnemyPBar);
        extraArmor = i.getIntExtra("extraArmor", 0);
        extraDamage = i.getIntExtra("extraDamage", 0);
        extraHealth = i.getIntExtra("extraHealth", 0);

        for(int i = 0; i <= heroChar.getLevel();i++){
            if(i < 5){
                extraHealth = extraHealth + 5;
                extraArmor = extraArmor + 2;
                Glide.with(this).load(R.drawable.enemy_idle5).into(monsterImage);
            }
            else if(i == 5){
                questionDifficulty = "medium";
            }
            else if(i <= 10){
                extraHealth = extraHealth + 10;
                extraArmor = extraArmor + 4;
                extraDamage = extraDamage + 2;
                Glide.with(this).load(R.drawable.enemy_idle3).into(monsterImage);
            }
            else if(i == 11) {
                questionDifficulty = "hard";
            }
            else if(i < 20){
                extraHealth = extraHealth + 15;
                extraArmor = extraArmor + 8;
                extraDamage = extraDamage + 4;
                Glide.with(this).load(R.drawable.enemy_idle2).into(monsterImage);
            }
            else if(i == 20){
                extraDamage = 80;
            }
        }
        int monsterHealth = rand.nextInt(100);
        enemyChar = new Character(monsterHealth + extraHealth, extraArmor + 30, extraDamage + 20, false);

        Log.e("e", "" + enemyChar.getmHealth());
        Log.e("e", "" + enemyChar.getmArmor());
        Log.e("e", "" + enemyChar.getmDamage());

        healthMonsterBar.setMax(enemyChar.getmHealth());
        healthMonsterBar.setProgress(enemyChar.getmHealth());
        Log.d("Monster Health", "" + enemyChar.getmHealth());

        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

    }

    public void getQuestion() {
        questionDifficulty = "easy";
        RequestQueue queue = Volley.newRequestQueue(this);
        String url ="https://opentdb.com/api.php?amount=10&difficulty=" + questionDifficulty + "&type=multiple";
        final int questionNum = rand.nextInt(11);


        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        String question;
                        String[] wrongAnswer = new String[3];
                        String correctAnswer;

                        try{
                            JSONObject obj = new JSONObject(response);
                            JSONArray questionArray = obj.getJSONArray("results");
                            JSONObject questionJSON = questionArray.getJSONObject(questionNum);
                            JSONArray wrongAnswerArray = questionJSON.getJSONArray("incorrect_answers");
                            try {
                                question = questionJSON.getString("question");
                                correctAnswer = questionJSON.getString("correct_answer");
                                for(int i = 0; i < wrongAnswer.length; i++){
                                    wrongAnswer[i] = wrongAnswerArray.getString(i);
                                }
                                String question1 = Html.fromHtml(question).toString();
                                String correctAnswer1 = Html.fromHtml(correctAnswer).toString();
                                String wrongAnswer1 = Html.fromHtml(wrongAnswer[0]).toString();
                                String wrongAnswer2 = Html.fromHtml(wrongAnswer[1]).toString();
                                String wrongAnswer3 = Html.fromHtml(wrongAnswer[2]).toString();
                                answers.clear();
                                answers.add(0, correctAnswer1);
                                answers.add(1, wrongAnswer1);
                                answers.add(2, wrongAnswer2);
                                answers.add(3, wrongAnswer3);

                                Collections.shuffle(answers);

                                //Delete if tests are needed
                                correctAns.clear();

                                correctAns.add(answers.indexOf(correctAnswer1));

                                Log.e("e", "" + correctAns.get(0));

                                questionTextView.setText(question1);
                                answerATextView.setText(answers.get(0));
                                answerBTextView.setText(answers.get(1));
                                answerCTextView.setText(answers.get(2));
                                answerDTextView.setText(answers.get(3));
                            }
                            catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "fail",Toast.LENGTH_SHORT).show();
            }
        });

        queue.add(stringRequest);
    }

    public void alert(View view) {
        int trueCorrectAnswer = correctAns.get(0);
        switch (view.getId()){
            case R.id.answerA_btn:
                if (answerATextView.getText().equals(answers.get(correctAns.get(0)))){
                    showCorrectAnswer(0);
                    didHeroWin(true);
                }
                else {
                    showIncorrectAnswer(trueCorrectAnswer);
                    didHeroWin(false);
                }
                break;
            case R.id.answerB_btn:
                if (answerBTextView.getText().equals(answers.get(correctAns.get(0)))){
                    showCorrectAnswer(1);
                    didHeroWin(true);
                }
                else {
                    showIncorrectAnswer(trueCorrectAnswer);
                    didHeroWin(false);
                }
                break;
            case R.id.answerC_btn:
                if (answerCTextView.getText().equals(answers.get(correctAns.get(0)))){
                    showCorrectAnswer(2);
                    didHeroWin(true);

                }
                else {
                    showIncorrectAnswer(trueCorrectAnswer);
                    didHeroWin(false);
                }
                break;
            case R.id.answerD_btn:
                if (answerDTextView.getText().equals(answers.get(correctAns.get(0)))){
                    showCorrectAnswer(3);
                    didHeroWin(true);
                }
                else {
                    showIncorrectAnswer(trueCorrectAnswer);
                    didHeroWin(false);
                }
                break;
        }

    }

    public void showCorrectAnswer(int correct) {
        answerATextView.setClickable(false);
        answerBTextView.setClickable(false);
        answerCTextView.setClickable(false);
        answerDTextView.setClickable(false);

        correctIndex = correct;
        toast = Toast.makeText(this, "Correct Answer has been clicked !!!", Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER,0,200);
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                toast.show();
                setCorrectAnswer(correctIndex);
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
        resetAnswers();
    }

    public void showIncorrectAnswer(int correct) {
        answerATextView.setClickable(false);
        answerBTextView.setClickable(false);
        answerCTextView.setClickable(false);
        answerDTextView.setClickable(false);

        correctIndex = correct;
        toast = Toast.makeText(this, "Wrong Answer was clicked.", Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER,0,200);
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                toast.show();
                setCorrectAnswer(correctIndex);
            }
        });
        thread.start();
        resetAnswers();
    }

    public void resetAnswers(){
        final Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(5500);
                    answerATextView.setTextColor(Color.BLACK);
                    answerBTextView.setTextColor(Color.BLACK);
                    answerCTextView.setTextColor(Color.BLACK);
                    answerDTextView.setTextColor(Color.BLACK);
                    getQuestion();
                    answerATextView.setClickable(true);
                    answerBTextView.setClickable(true);
                    answerCTextView.setClickable(true);
                    answerDTextView.setClickable(true);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
    }

    public void setCorrectAnswer(int correct) {
        switch (correct) {
            case 0:
                answerATextView.setTextColor(Color.parseColor("#228b22"));
                answerBTextView.setTextColor(Color.RED);
                answerCTextView.setTextColor(Color.RED);
                answerDTextView.setTextColor(Color.RED);
                break;
            case 1:
                answerATextView.setTextColor(Color.RED);
                answerBTextView.setTextColor(Color.parseColor("#228b22"));
                answerCTextView.setTextColor(Color.RED);
                answerDTextView.setTextColor(Color.RED);
                break;
            case 2:
                answerATextView.setTextColor(Color.RED);
                answerBTextView.setTextColor(Color.RED);
                answerCTextView.setTextColor(Color.parseColor("#228b22"));
                answerDTextView.setTextColor(Color.RED);
                break;
            case 3:
                answerATextView.setTextColor(Color.RED);
                answerBTextView.setTextColor(Color.RED);
                answerCTextView.setTextColor(Color.RED);
                answerDTextView.setTextColor(Color.parseColor("#228b22"));
                break;
        }
    }

    public void didHeroWin(boolean didWin){
        Intent i;

        if(didWin){
            enemyChar.receiveAttack(heroChar.getmDamage(), enemyChar.getmArmor());
            healthMonsterBar.setProgress(enemyChar.getmCurrentHealth());
            if(enemyChar.getmCurrentHealth() == 0){
                Log.e("ok", "winner");
                AlertDialog.Builder alertDialog = new
                        AlertDialog.Builder(LevelOneScreen.this);
                alertDialog.setTitle("Winner");
                alertDialog.setMessage("You defeated the monster!");
                alertDialog.show();
                heroChar.gainPoints();
                heroChar.setCurrency(heroChar.getCurrency() + 10);
                i= new Intent(this, ShopScreen.class);
                i.putExtra("heroChar", heroChar);
                i.putExtra("extraArmor",extraArmor);
                i.putExtra("extraDamage",extraDamage);
                i.putExtra("extraHealth",extraHealth);
                heroChar.setLevel(heroChar.getLevel() + 1);
                startActivity(i);
                Log.d("LOGGING", "Hero current " + heroChar.getmCurrentHealth() + " and max " + heroChar.getmHealth() + " in Level activity");
            }
        }
        else{
            heroChar.receiveAttack(enemyChar.getmDamage(), heroChar.getmArmor());
            healthHeroBar.setProgress(heroChar.getmCurrentHealth());
            if(heroChar.getmCurrentHealth() == 0){
                AlertDialog.Builder alertDialog = new
                        AlertDialog.Builder(LevelOneScreen.this);
                alertDialog.setTitle("Loser");
                alertDialog.setMessage("You lost to the monster!");
                alertDialog.show();
                i = new Intent(this, DeathScreen.class);
                i.putExtra("heroChar", heroChar);
                startActivity(i);
            }
        }
    }

    @Override
    public void onBackPressed() {
        Log.d("DEBUG Level 1 SCREEN", "got inside the onBackPressed");
        Intent i = new Intent();
        i.putExtra("heroChar", heroChar);
        setResult(100, i);
    }
}
