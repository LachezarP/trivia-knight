package com.example.triviaknight;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ImageView heroImage = findViewById(R.id.heroImg);

        Glide.with(this).load(R.drawable.knight_idle).into(heroImage);
    }

    public void goToSelectScreen(View view) {
        Intent selectScreen = new Intent(this, SelectScreen.class);
        Character hero = new Character(100,10,20,true);
        selectScreen.putExtra("heroChar", hero);
        startActivity(selectScreen);
    }
}
