package com.example.triviaknight;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.LinkedList;

public class ShopAdapter extends RecyclerView.Adapter<ShopAdapter.ShopViewHolder> {

    public class ShopViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public final TextView description;
        public final TextView price;
        public final ImageView image;
        public final TextView quantityText;
        final ShopAdapter adapter;
        Button addButton;
        Button removeButton;

        public ShopViewHolder(View itemView, ShopAdapter adapter) {
            super(itemView);
            description = itemView.findViewById(R.id.description_textView);
            price = itemView.findViewById(R.id.price_textView);
            image = itemView.findViewById(R.id.imageView);
            quantityText = itemView.findViewById(R.id.quantity_textView);
            this.adapter = adapter;

            addButton = itemView.findViewById(R.id.add_button);
            removeButton = itemView.findViewById(R.id.remove_button);
            addButton.setOnClickListener(this);
            removeButton.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.add_button: add();
                    break;
                case R.id.remove_button: remove();
                    break;
            }
        }

        public void add() {
            int position = getLayoutPosition();

            Item current = itemList.get(position);
            int quantity;

            quantity = current.getmQuantity() + 1;
            current.setmQuantity(quantity);
            quantityText.setText(quantity+"");
        }

        public void remove() {
            int position = getLayoutPosition();

            Item current = itemList.get(position);
            int quantity = current.getmQuantity();

            if (quantity > 0) {
                quantity--;
                current.setmQuantity(quantity);
                quantityText.setText(quantity +"");
            }
        }
    }

    private final ArrayList<Item> itemList;
    private LayoutInflater inflater;

    public ShopAdapter(Context context, ArrayList<Item> itemList) {
        inflater = LayoutInflater.from(context);
        this.itemList = itemList;
    }

    @NonNull
    @Override
    public ShopAdapter.ShopViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = inflater.inflate(R.layout.item_list, parent, false);
        return new ShopViewHolder(itemView, this);
    }

    @Override
    public void onBindViewHolder(@NonNull ShopAdapter.ShopViewHolder holder, int position) {
        Item current = itemList.get(position);
        holder.description.setText(current.getmDescription());
        holder.price.setText(current.getmPrice());
        holder.image.setImageResource(current.getmImage());
        holder.quantityText.setText(current.getmQuantity()+"");
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }
}
