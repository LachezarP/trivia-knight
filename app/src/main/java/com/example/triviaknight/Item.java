package com.example.triviaknight;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;


public class Item implements Serializable {

    private int mPrice;
    private int mQuantity;//R.string.itemCard_initialQuantity;
    private int mType;
    private int mDescription;
    private int mImage;

    public Item(int mType, int mQuantity) {
        this.mType = mType;
        this.mQuantity = mQuantity;
        switch(mType){
            case 0:
                mPrice = R.string.lesserHealingPotionPrice;
                mDescription = R.string.lesserHealingPotionStr;
                mImage = R.drawable.health;
                break;
            case 1:
                mPrice = R.string.lesserDamagePotionPrice;
                mDescription = R.string.lesserDamagePotionStr;
                mImage = R.drawable.attack;
                break;
            case 2:
                mPrice = R.string.greaterHealingPotionPrice;
                mDescription = R.string.greaterHealingPotionStr;
                mImage = R.drawable.health;
                break;
            case 3:
                mPrice = R.string.greaterDamagePotionPrice;
                mDescription = R.string.greaterDamagePotionStr;
                mImage = R.drawable.attack;
                break;
            default:
                mPrice = 15;
                mDescription = R.string.lesserHealingPotionStr;
                mImage = R.drawable.health;
        }
    }

    protected Item(Parcel in) {
        mPrice = in.readInt();
        mQuantity = in.readInt();
        mType = in.readInt();
        mDescription = in.readInt();
        mImage = in.readInt();
    }


    public void doItemEffect(Character hero, Character enemy){
        switch (mType){
            case 0:
                hero.setmCurrentHealth(hero.getmCurrentHealth() + 10);
                break;
            case 1:
                enemy.receiveAttack(10);
                break;
            case 2:
                hero.setmCurrentHealth(hero.getmCurrentHealth() + 30);
                break;
            case 3:
                enemy.receiveAttack(30);
                break;
        }
    }

    public int getmPrice() {
        return mPrice;
    }

    public void setmPrice(int mPrice) {
        this.mPrice = mPrice;
    }

    public int getmQuantity() {
        return mQuantity;
    }

    public void setmQuantity(int mQuantity) {
        this.mQuantity = mQuantity;
    }

    public int getmType() {
        return mType;
    }

    public void setmType(int mType) {
        this.mType = mType;
    }

    public int getmDescription() {
        return mDescription;
    }

    public void setmDescription(int mDescription) {
        this.mDescription = mDescription;
    }

    public int getmImage() {
        return mImage;
    }

    public void setmImage(int mImage) {
        this.mImage = mImage;
    }

//    @Override
//    public int describeContents() {
//        return 0;
//    }
//
//    @Override
//    public void writeToParcel(Parcel dest, int flags) {
//        dest.writeInt(mPrice);
//        dest.writeInt(mQuantity);
//        dest.writeInt(mType);
//        dest.writeInt(mDescription);
//        dest.writeInt(mImage);
//    }
}
