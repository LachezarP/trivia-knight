package com.example.triviaknight;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class DeathScreen extends AppCompatActivity {

    Character heroChar;
    Intent i;
    private TextView healthText;
    private TextView damageText;
    private TextView armorText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_death_screen);

        i = getIntent();
        heroChar = (Character)i.getSerializableExtra("heroChar");

        healthText = findViewById(R.id.health_text);
        damageText = findViewById(R.id.damage_text);
        armorText = findViewById(R.id.armor_text);

        healthText.setText(heroChar.getmHealth() + "");
        damageText.setText(heroChar.getmDamage() + "");
        armorText.setText(heroChar.getmArmor() + "");
    }

    public void goToSelectScreen(View view) {
        Intent selectScreen = new Intent(this, MainActivity.class);
        startActivity(selectScreen);
    }
}
