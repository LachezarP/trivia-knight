package com.example.triviaknight;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Upgrade_Screen extends AppCompatActivity {

    Intent i;
    Character heroChar;
    ArrayList<Item> itemList;
    private int totalPoints;
    private int armorPoints;
    private int healthPoints;
    private int damagePoints;
    private int upgradePoints;
    private TextView healthAmount;
    private TextView armorAmount;
    private TextView damageAmount;
    private TextView upgradeAmount;
    private Button nextLevelButton;
    int extraArmor;
    int extraDamage;
    int extraHealth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upgrade__screen);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        i = getIntent();
        heroChar = (Character)i.getSerializableExtra("heroChar");
        extraArmor = i.getIntExtra("extraArmor", 0);
        extraDamage = i.getIntExtra("extraDamage", 0);
        extraHealth = i.getIntExtra("extraHealth", 0);
        itemList = (ArrayList<Item>)i.getSerializableExtra("itemList");

        totalPoints = heroChar.getUpgradePoints();
        armorPoints = heroChar.getTotalArmorPoints();
        healthPoints = heroChar.getTotalHealthPoints();
        damagePoints = heroChar.getTotalDamagePoints();
        upgradePoints = heroChar.getUpgradePoints();

        healthAmount = findViewById(R.id.healthAmount_text);
        armorAmount = findViewById(R.id.armorPoints_text);
        damageAmount = findViewById(R.id.damagePoint_text);
        upgradeAmount = findViewById(R.id.upgradePoint_text);
        nextLevelButton = findViewById(R.id.nextLevelButton);

        healthAmount.setText(healthPoints + "");
        armorAmount.setText(armorPoints + "");
        damageAmount.setText(damagePoints + "");
        upgradeAmount.setText(getResources().getString(R.string.upgradePointsDisplay_part1) + " "+
                upgradePoints + " " + getResources().getString(R.string.upgradePointsDisplay_part2));
        ifAllPointsUsed();

        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

    }

    public void goToUpgrade(View view){
        i= new Intent(this, SelectScreen.class);
        i.putExtra("heroChar", heroChar);
        startActivity(i);
    }

    public void addPoint(View view){
        if (view.equals(findViewById(R.id.addHealth_button)) && heroChar.getUpgradePoints() > 0) {
            heroChar.setmHealth(heroChar.getmHealth() + 20);
            heroChar.setmHealth(heroChar.getmCurrentHealth() + 20);
            heroChar.setTotalHealthPoints(heroChar.getTotalHealthPoints() + 1);
            heroChar.setUpgradePoints(heroChar.getUpgradePoints() - 1);
            upgradeAmount.setText(getResources().getString(R.string.upgradePointsDisplay_part1) +
                    " " + heroChar.getUpgradePoints() + " " +
                    getResources().getString(R.string.upgradePointsDisplay_part2));
            healthAmount.setText(heroChar.getTotalHealthPoints() + "");
            ifAllPointsUsed();
        }
        if (view.equals(findViewById(R.id.addAttack_button)) && heroChar.getUpgradePoints() > 0){
            heroChar.setmDamage(heroChar.getmDamage() + 5);
            heroChar.setTotalDamagePoints(heroChar.getTotalDamagePoints() + 1);
            heroChar.setUpgradePoints(heroChar.getUpgradePoints() - 1);
            upgradeAmount.setText(getResources().getString(R.string.upgradePointsDisplay_part1) +
                    " " + heroChar.getUpgradePoints() + " " +
                    getResources().getString(R.string.upgradePointsDisplay_part2));
            damageAmount.setText(heroChar.getTotalDamagePoints() + "");
            ifAllPointsUsed();
        }
        if (view.equals(findViewById(R.id.addArmor_button)) && heroChar.getUpgradePoints() > 0) {
            heroChar.setmArmor(heroChar.getmArmor() + 2);
            heroChar.setTotalArmorPoints(heroChar.getTotalArmorPoints() + 1);
            heroChar.setUpgradePoints(heroChar.getUpgradePoints() - 1);
            upgradeAmount.setText(getResources().getString(R.string.upgradePointsDisplay_part1) +
                    " " + heroChar.getUpgradePoints() + " " +
                    getResources().getString(R.string.upgradePointsDisplay_part2));
            armorAmount.setText(heroChar.getTotalArmorPoints() + "");
            ifAllPointsUsed();
            Log.d("LOGGIN", " the total health points" + heroChar.getTotalHealthPoints());
            Log.d("LOGGIN", " the total health points" + heroChar.getmHealth());
        }
    }

    public void removePoint(View view) {
        if (view.equals(findViewById(R.id.removeHealth_button)))
            if (heroChar.getTotalHealthPoints() > 0){
                heroChar.setmHealth(heroChar.getmHealth() - 20);
                heroChar.setmHealth(heroChar.getmCurrentHealth() - 20);
                heroChar.setTotalHealthPoints(heroChar.getTotalHealthPoints() - 1);
                heroChar.setUpgradePoints(heroChar.getUpgradePoints() + 1);
                upgradeAmount.setText(getResources().getString(R.string.upgradePointsDisplay_part1) +
                        " " + heroChar.getUpgradePoints() + " " +
                        getResources().getString(R.string.upgradePointsDisplay_part2));
                healthAmount.setText(heroChar.getTotalHealthPoints() +"");
                ifAllPointsUsed();
            }
        if (view.equals(findViewById(R.id.removeAttack_button)))
            if (heroChar.getTotalDamagePoints() > 0){
                heroChar.setmDamage(heroChar.getmDamage() - 5);
                heroChar.setTotalDamagePoints(heroChar.getTotalDamagePoints() - 1);
                heroChar.setUpgradePoints(heroChar.getUpgradePoints() + 1);
                upgradeAmount.setText(getResources().getString(R.string.upgradePointsDisplay_part1) +
                        " " + heroChar.getUpgradePoints() + " " +
                        getResources().getString(R.string.upgradePointsDisplay_part2));
                damageAmount.setText(heroChar.getTotalDamagePoints() + "");
                ifAllPointsUsed();
            }
        if (view.equals(findViewById(R.id.removeArmor_button)))
            if (heroChar.getTotalArmorPoints() > 0){
                heroChar.setmArmor(heroChar.getmArmor() - 2);
                heroChar.setTotalArmorPoints(heroChar.getTotalArmorPoints() - 1);
                heroChar.setUpgradePoints(heroChar.getUpgradePoints() + 1);
                upgradeAmount.setText(getResources().getString(R.string.upgradePointsDisplay_part1) +
                        " " + heroChar.getUpgradePoints() + " " +
                        getResources().getString(R.string.upgradePointsDisplay_part2));
                armorAmount.setText(heroChar.getTotalArmorPoints() + "");
                ifAllPointsUsed();
            }
    }

    public void ifAllPointsUsed(){
        if(heroChar.getUpgradePoints() > 0){
            nextLevelButton.setEnabled(false);
        }
        else
            nextLevelButton.setEnabled(true);
    }

    public void goToNextLevel(View view){
        i= new Intent(this, SelectScreen.class);
        i.putExtra("heroChar", heroChar);
        i.putExtra("extraArmor",extraArmor);
        i.putExtra("extraDamage",extraDamage);
        i.putExtra("extraHealth",extraHealth);
        i.putExtra("itemList", itemList);
        startActivity(i);
    }

}
