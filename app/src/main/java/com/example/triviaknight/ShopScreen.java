package com.example.triviaknight;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.Objects;

public class ShopScreen extends AppCompatActivity implements Serializable {
    Intent i;
    Character heroChar;
    ArrayList<Item> itemList = new ArrayList<>();
    private RecyclerView recyclerView;
    private ShopAdapter adapter;
    int extraArmor;
    int extraDamage;
    int extraHealth;
    TextView currencyText;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_screen);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        setItems();
        currencyText = findViewById(R.id.currentMoney);

        recyclerView = findViewById(R.id.item_recyclerView);
        adapter = new ShopAdapter(this, itemList);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        i = getIntent();
        heroChar = (Character)i.getSerializableExtra("heroChar");
        extraArmor = i.getIntExtra("extraArmor", 0);
        extraDamage = i.getIntExtra("extraDamage", 0);
        extraHealth = i.getIntExtra("extraHealth", 0);
        Log.d("LOGGING", "Hero current " + heroChar.getmCurrentHealth() + " and max " + heroChar.getmHealth() + " in Level activity");
        currencyText.setText(String.format("Current gold : %d", heroChar.getCurrency()));
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

    }

    public void goToUpgrade(View view){
        Log.d("Testing", "testing quantitiy : " + itemList.get(0).getmQuantity() + " " + itemList.get(1).getmQuantity());
//        ArrayList<Item> arrayItem = new ArrayList<>(itemList);
        i= new Intent(this, Upgrade_Screen.class);
        i.putExtra("heroChar", heroChar);
        i.putExtra("extraArmor",extraArmor);
        i.putExtra("extraDamage",extraDamage);
        i.putExtra("extraHealth",extraHealth);
        i.putExtra("itemList", itemList);
        startActivity(i);
    }

    private void setItems() {
        itemList.add(new Item(0, 0));
        itemList.add(new Item(1, 0));
        itemList.add(new Item(2, 0));
        itemList.add(new Item(3, 0));
    }
}
