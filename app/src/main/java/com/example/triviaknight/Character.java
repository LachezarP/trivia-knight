package com.example.triviaknight;

import java.io.Serializable;

// TODO
// add image
public class Character implements Serializable {

    private int mHealth; // variable for the initial health of the character
    private int mArmor; // variable for the armor of the character
    private int mDamage; // variable for the damage of the character
    private boolean isHero; // variable to check if the character is a monster or a hero
    private int mCurrentHealth; // variable to check the current health of the character
    private int upgradePoints = 0;  // variable that users can use to upgrade attributes
    private int totalArmorPoints = 0; // variable that keeps track of Armor
    private int totalHealthPoints = 0; // variable that keeps track of Health
    private int totalDamagePoints = 0; // variable that keeps track of Damage
    private final int POINTS_GAINED = 3; // variable for the points gained after hero wins level
    private int level;


    private int currency;

    public Character(int mHealth, int mArmor, int mDamage, boolean isHero) {
        this.mHealth = mHealth;
        this.mArmor = mArmor;
        this.mDamage = mDamage;
        this.isHero = isHero;
        mCurrentHealth = mHealth;
        level = 1;
        currency = 0;
    }

    /**
     * Method to receive an attack from another character
     * @param attackDamage the attack damage of the attacker
     */
    public void receiveAttack(int attackDamage, int armor) {
        int totalDamage;
        if(armor > attackDamage)
            totalDamage = attackDamage / 2;
        else
            totalDamage = attackDamage - armor;

        if (this.mCurrentHealth - totalDamage < 0)
            this.mCurrentHealth = 0;
        else
            this.mCurrentHealth -= totalDamage;
    }
    /**
     * Method to receive an attack from another character
     * @param damage the attack damage of the attacker
     */
    public void receiveAttack(int damage) {

        if (this.mCurrentHealth - damage < 0)
            this.mCurrentHealth = 0;
        else
            this.mCurrentHealth -= damage;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public void gainPoints() {
        upgradePoints += POINTS_GAINED;
    }

    /**
     * Method to return the current health of the character
     * @return current health
     */
    public int getmCurrentHealth() {
        return mCurrentHealth;
    }

    /**
     * Method to set the current health
     * @param mCurrentHealth the current health
     */
    public void setmCurrentHealth(int mCurrentHealth) {
        this.mCurrentHealth = mCurrentHealth;
    }

    /**
     * Method to return the health of the character
     * @return the health
     */
    public int getmHealth() {
        return mHealth;
    }

    /**
     * Method to set the health of the character
     */
    public void setmHealth(int mHealth) {
        this.mHealth = mHealth;
    }

    /**
     * Method to return the armor of the character
     * @return the armor
     */
    public int getmArmor() {
        return mArmor;
    }
    /**
     * Method to set the armor of the character
     */
    public void setmArmor(int mArmor) {
        this.mArmor = mArmor;
    }

    /**
     * Method to return the health of the character
     * @return damage
     */
    public int getmDamage() {
        return mDamage;
    }
    /**
     * Method to set the damage of the character
     */
    public void setmDamage(int mDamage) {
        this.mDamage = mDamage;
    }

    /**
     * Method to return if character is a hero
     * @return if it is a hero
     */
    public boolean isHero() {
        return isHero;
    }
    /**
     * Method to set if the character is a hero
     */
    public void setHero(boolean hero) {
        isHero = hero;
    }

    public int getUpgradePoints() {
        return upgradePoints;
    }

    public void setUpgradePoints(int upgradePoints) {
        this.upgradePoints = upgradePoints;
    }

    public int getTotalArmorPoints() {
        return totalArmorPoints;
    }

    public void setTotalArmorPoints(int totalArmorPoints) {
        this.totalArmorPoints = totalArmorPoints;
    }

    public int getTotalHealthPoints() {
        return totalHealthPoints;
    }

    public void setTotalHealthPoints(int totalHealthPoints) {
        this.totalHealthPoints = totalHealthPoints;
    }

    public int getTotalDamagePoints() {
        return totalDamagePoints;
    }

    public void setTotalDamagePoints(int totalDamagePoints) {
        this.totalDamagePoints = totalDamagePoints;
    }
    public int getCurrency() {
        return currency;
    }

    public void setCurrency(int currency) {
        this.currency = currency;
    }

}
